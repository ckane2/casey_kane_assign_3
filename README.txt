Name: Casey Kane
Email: ckane2@binghamton.edu

----------------------------------------------------------
## To clean:
ant -buildfile src/build.xml clean

----------------------------------------------------------
## To compile:
ant -buildfile src/build.xml all

Please compile before run. Needed to get BUILD and
BUILD/classes directories in directory structure
----------------------------------------------------------
## To run by specifying arguments from command line
ant -buildfile src/build.xml run -Darg0='path/to/input.txt' -Darg1='path/to/output.txt'

----------------------------------------------------------
## To create tarball for submission
ant -buildfile src/build.xml tarzip

----------------------------------------------------------
## Academic Honesty Statement

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense."

[Date: ] -- 10/19/17

----------------------------------------------------------
## Algorithm Note

I calculate the new statistics for each new traveller before changing state.
Additionally, I use the total days to determine the state of the airport instead
of basing the statistics off a single day.

----------------------------------------------------------
## Citations

